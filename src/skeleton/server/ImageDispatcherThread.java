package skeleton.server;

import java.io.DataOutputStream;
import java.io.IOException;

import se.lth.cs.eda040.fakecamera.AxisM3006V;

public class ImageDispatcherThread extends Thread {
	private AxisM3006V camera;
	private Connection connection;
	private long lastSend;
	private String proxyname;
	private int port;
	
	public ImageDispatcherThread(Connection connection, String proxy, int port){
		this.connection = connection;
		camera = new AxisM3006V();
		camera.init();
		this.proxyname = proxy;
		this.port = port;
	}

	public void run(){		
		camera.setProxy(proxyname, port);
		camera.connect();
		while(true){
			DataOutputStream output = connection.getOutputStream();
			try{
				System.out.println("Received an output stream");
				while(true){
					ImagePackage message = new ImagePackage();
					message.imageBuff = new byte[AxisM3006V.IMAGE_BUFFER_SIZE];
					message.imageSize = camera.getJPEG(message.imageBuff, 0);
					byte[] ts = new byte[AxisM3006V.TIME_ARRAY_SIZE];
					camera.getTime(ts, 0);
					long lts = 0;
					for (int i = 0; i < ts.length; i++)
					{
						lts = (lts << 8) + (ts[i] & 0xff);
					}
					message.timeStamp = lts;
					message.motionDetected = camera.motionDetected();
					if(message.motionDetected && connection.getMovieMode() == Server.AUTO_MODE){
						connection.setMovieMode(Server.VIDEO_MODE);
					}
					if(connection.getMovieMode() == Server.VIDEO_MODE || message.timeStamp - lastSend > 5000 ){
						lastSend = message.timeStamp;
						output.writeBoolean(message.motionDetected);
						output.writeInt(message.imageSize);
						output.writeLong(message.timeStamp);
						output.write(message.imageBuff, 0, message.imageSize);
						output.flush();
					}
				}
			}catch(IOException e){
				System.out.println("Broken output stream");
				connection.tearDown(output);
			}
		}
	}
}