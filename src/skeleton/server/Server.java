package skeleton.server;

public class Server{
	public final static int VIDEO_MODE = 0;
	public final static int IDLE_MODE = 1;
	public final static int AUTO_MODE = 2;
	
	private ImageDispatcherThread id;
	private MessageHandlerThread mt;
	private Connection connection;
	
	public static void main(String []args){
		Server server = new Server();
		String proxy = "argus-1.student.lth.se";
		int port = 6077+(int)(Math.random()*10);
		System.out.println(port);
		if(args.length >= 1){
			port = Integer.parseInt(args[0]);
		}
		if(args.length == 2){
			proxy = args[1];
		}
		server.run(proxy, port);
	}
	
	public void run(String proxy, int port){
		connection = new Connection();
		
		id = new ImageDispatcherThread(connection, proxy, port);
		mt = new MessageHandlerThread(connection);
		
		mt.start();
		id.start();
		
		while(true){
			connection.buildUp(port);
		}
	}
}
