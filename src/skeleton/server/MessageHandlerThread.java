package skeleton.server;

import java.io.DataInputStream;
import java.io.IOException;

public class MessageHandlerThread extends Thread {
	private Connection connection;
	
	public MessageHandlerThread(Connection connection){
		this.connection = connection;
	}
	
	public void run(){
		while(true){
			//Get an input thread
			//while true, handle messages
			DataInputStream input = connection.getInputStream();
			try{
				while(true){
					System.out.println("Input stream received, waiting for input...");
					int mode = input.readInt();
					System.out.println("Received a mode message "+mode);
					connection.setMovieMode(mode);
				}
			}catch(IOException e){
				System.out.println("Broken input stream");
				connection.tearDown(input);
			}
		}
	}
}