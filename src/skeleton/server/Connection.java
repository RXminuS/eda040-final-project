package skeleton.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Connection {
	private ServerSocket ss = null;
	private Socket client = null;
	private DataInputStream is = null;
	private DataOutputStream os = null;
	private int movieMode = Server.AUTO_MODE;
	
	synchronized public void setMovieMode(int mode){
		movieMode = mode;
		notifyAll();
	}
	
	synchronized public int getMovieMode(){
		return movieMode;
	}
	
	synchronized public void buildUp(int port){
		while(ss != null){
			try {
				wait();
			} catch (InterruptedException e1) {
				throw new RuntimeException();
			}
		}
		try {
			ss = new ServerSocket(port, 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			client = ss.accept(); //Blocking Function that waits for a client to connect
			//client.setKeepAlive(true); //Makes the socket not auto timeout
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		movieMode = Server.AUTO_MODE;
		notifyAll();
	}
	
	private void tearDown(){
		System.out.println("Tearing down this shit");
		client = null;
		os = null;
		is = null;
		try {
			if(ss != null) ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ss = null;
		notifyAll();
	}
	
	synchronized public void tearDown(DataOutputStream output){
		//Check that the thread holding a reference had the latest version available
		//otherwise simply ignore the tearDown request
		if(this.os == output){
			tearDown();
		}
	}	
	
	synchronized public void tearDown(DataInputStream input){
		//Check that the thread holding a reference had the latest version available
		//otherwise simply ignore the tearDown request
		if(this.is == input){
			tearDown();
		}
	}
	
	private boolean connectionReady(){
		return (client != null);
	}
	
	synchronized public DataOutputStream getOutputStream(){
		System.out.println("getting Output Stream");
		while(!connectionReady() ){
			try {
				wait();
			} catch (InterruptedException e) {
				throw new RuntimeException();
			}
		}
		
		try {
			os = new DataOutputStream(client.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return os;
	}
	
	
	
	synchronized public DataInputStream getInputStream(){
		System.out.println("getting Input Stream");

			while(!connectionReady() ){
				try {
					wait();
				} catch (InterruptedException e) {
					throw new RuntimeException();
				}
			}

		try {
			is = new DataInputStream(client.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return is;
	}
}