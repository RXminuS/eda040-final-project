package skeleton.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import skeleton.server.ImagePackage;
import skeleton.server.Server;

public class Connection {
	private Socket server[] = new Socket[2];
	private DataOutputStream os[] = new DataOutputStream[2];
	private DataInputStream is[] = new DataInputStream[2];
	
	public final static int MAX_SERVER_ID = 1;
	private int clientMode = Server.IDLE_MODE;
	
	synchronized public void buildUp(String host, int port, int id){
		System.out.println("Starting a connection with "+host+":"+port);
		try{
			server[id] = new Socket(host, port);
			os[id] = new DataOutputStream(server[id].getOutputStream());
			is[id] = new DataInputStream(server[id].getInputStream());
		}catch(Exception e ){
			tearDown(id);
			throw new NullPointerException("Could not connect to server");
		}
		//Check if the pipeline was successfully opened
		notifyAll();
	}
	
	synchronized public void tearDown(int id){
		try {
			os[id] = null;
			is[id] = null;
			if(server[id] != null) server[id].close();
			server[id] = null;
		} catch (IOException e) {
			
		}
		notifyAll();
	}
	
	/**
	 * @return the ID of the camera whose header information has been entered in the ImagePackage or -1
	 * @throws InterruptedException
	 */
	synchronized public int getMessage(ImagePackage header) throws InterruptedException{
			while(!connectionReady()) wait();
			//Check all ready connections for a new message
			for(int i = 0; i <= MAX_SERVER_ID; i++){
				if(connectionReady(i)){
					//Check if the bytes available for reading equal the header size (header has arrived)
					try {
						if(is[i].available() >= 5 /*one motion detection bit one image size integer*/){
							header.motionDetected = is[i].readBoolean();
							header.imageSize = is[i].readInt();
							return i;
						}
					} catch (IOException e) {
						//This connection is dead, gracefully shut it down and continue searching for a message
						tearDown(i);
					}
				}
			}
			
			return -1;
	}
	
	synchronized public void getImageData(ImagePackage header, int id) throws IOException{
		if(!connectionReady(id))
			throw new IllegalArgumentException("The connection isn't open");
		
		header.imageBuff = new byte[header.imageSize];
		header.timeStamp = is[id].readLong();
		is[id].readFully(header.imageBuff, 0, header.imageSize);	
	}
	
	synchronized public void propagateMotionDetected(){
		switch(clientMode){
			case Server.AUTO_MODE:
				setMode(Server.VIDEO_MODE);
				break;
			case Server.VIDEO_MODE:
				break;
		}
	}
	
	private boolean connectionReady(){
		return (server[0] != null || server[1] != null);
	}
	
	private boolean connectionReady(int id){
		return os[id] != null;
	}
	
	synchronized public void setMode(int mode){
		clientMode = mode;
		for(int i = 0; i <= MAX_SERVER_ID; i++){
			if(connectionReady(i)){
				try {
					os[i].writeInt(mode);
					os[i].flush();
				} catch (IOException e) {
					tearDown(i);
				}
			}
		}
	}
}