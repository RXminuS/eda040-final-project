package skeleton.client;

public class Client {
	private Connection connection;
	private SyncedImageBuffer buff;
	private ReceiverThread receiver;
	private GUIThread gui;
	
	//globals
	public static final long IMAGE_DELAY = 150;
	//globals end
	
	public static void main(String[] args){
		Client client = new Client();
		client.run();
	}
	
	public void run(){
		connection = new Connection();
		buff = new SyncedImageBuffer();
		//Start the receiver threads
		receiver = new ReceiverThread(connection, buff);
		receiver.start();
		gui = new GUIThread(connection, buff);
		gui.run();
		//Wait for messages
	}
}