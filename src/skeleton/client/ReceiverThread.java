package skeleton.client;

import java.io.IOException;

import skeleton.server.ImagePackage;

//import se.lth.cs.eda040.fakecamera.AxisM3006V;

public class ReceiverThread extends Thread {
	private Connection connection;
	private SyncedImageBuffer buff;
	
	public ReceiverThread(Connection connection, SyncedImageBuffer buff){
		this.connection = connection;
		this.buff = buff;
	}
	
	public void run(){
		while(true){
			ImagePackage message = new ImagePackage();
			try {
				int id = -1;
				while(id == -1){
					id = connection.getMessage(message);
				}
				//Handle any urgent messages like a MOTION DETECTED ETC
				if(message.motionDetected) connection.propagateMotionDetected();
				connection.getImageData(message, id);
				//Put image data in the synched ImageBuffer so the GUI thread can display it
				buff.putImage(message, id);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
