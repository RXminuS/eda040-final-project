package skeleton.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import skeleton.server.Server;

public class GUIThread extends Thread {
	private ImagePanel leftImage;
	private ImagePanel rightImage;
	private JLabel leftTopLabel;
	private JLabel rightTopLabel;
	private JTextField leftIPField;
	private JTextField rightIPField;
	private Connection con;
	private SyncedImageBuffer buff;
	private JLabel delayCamera1;
	private JLabel delayCamera2;

	public GUIThread(final Connection con, SyncedImageBuffer buff) {
		// initiating start arguments
		this.con = con;
		this.buff = buff;

		// creating frames and panels
		final JFrame frame = new JFrame("Camera Surveillance");
		// creating label panel
		JPanel topLabels = new JPanel(new FlowLayout());

		JPanel mainP = new JPanel();
		mainP.setLayout(new BoxLayout(mainP, BoxLayout.Y_AXIS));

		JPanel topP = new JPanel(new FlowLayout());

		JPanel delayPanel = new JPanel(new GridLayout(1, 2));
		delayCamera1 = new JLabel("Not Synchronized     Delay: ");
		delayCamera2 = new JLabel("Not Synchronized     Delay: ");
		delayCamera1.setHorizontalAlignment(SwingConstants.CENTER);
		delayCamera2.setHorizontalAlignment(SwingConstants.CENTER);
		delayPanel.add(delayCamera1);
		delayPanel.add(delayCamera2);

		JPanel botP = new JPanel(new FlowLayout());

		JPanel leftBotP = new JPanel();
		leftBotP.setLayout(new GridLayout(3, 1));
		leftBotP.setPreferredSize(new Dimension(600, 100));

		JPanel rightBotP = new JPanel();
		rightBotP.setLayout(new GridLayout(3, 1));
		rightBotP.setPreferredSize(new Dimension(600, 100));

		// topLabel
		final JButton idleModeButton = new JButton(" Idle Mode");
		idleModeButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				con.setMode(Server.IDLE_MODE);
			}
		});
		final JButton autoModeButton = new JButton(" Auto Mode");
		autoModeButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				con.setMode(Server.AUTO_MODE);
			}
		});
		final JButton movieModeButton = new JButton(" Movie Mode");
		movieModeButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				con.setMode(Server.VIDEO_MODE);
			}
		});

		topLabels.add(autoModeButton);
		topLabels.add(movieModeButton);
		topLabels.add(idleModeButton);

		// image windows

		leftImage = new ImagePanel();
		leftImage.setPreferredSize(new Dimension(600, 480));

		rightImage = new ImagePanel();
		rightImage.setPreferredSize(new Dimension(600, 480));

		// adding images to topPanel

		topP.add(leftImage);
		topP.add(rightImage);

		// IP Labels
		JLabel leftIPLabel = new JLabel("IP of camera 1:");

		JLabel rightIPLabel = new JLabel("IP of camera 2:");

		// IP textfields windows
		leftIPField = new JTextField("127.0.0.1:6077");
		rightIPField = new JTextField("127.0.0.1:6077");

		// button 1
		final JButton leftConnectButton = new JButton(" Connect Camera 1");
		final JButton leftDisconnectButton = new JButton(" Disconnect Camera 1");
		leftDisconnectButton.setEnabled(false);

		leftConnectButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					createConnection(0, getLeftFieldText());
				} catch (NullPointerException exception) {
					JOptionPane.showMessageDialog(frame,
							"Could not establish a connection!",
							"Connection Error", JOptionPane.ERROR_MESSAGE);
					return;
				} catch (IllegalArgumentException exception) {
					JOptionPane.showMessageDialog(frame,
							"The address provided was not a valid format!",
							"Format Error", JOptionPane.ERROR_MESSAGE);
					leftIPField.setText("172.0.0.1:6077");
					return;
				}
				leftConnectButton.setEnabled(false);
				leftDisconnectButton.setEnabled(true);
				leftIPField.setEnabled(false);
			}
		});

		leftDisconnectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				con.tearDown(0);
				leftConnectButton.setEnabled(true);
				leftDisconnectButton.setEnabled(false);
				leftIPField.setEnabled(true);
			}
		});
		leftConnectButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		leftDisconnectButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		leftConnectButton.setForeground(new Color(39, 174, 96));
		leftDisconnectButton.setForeground(new Color(231, 76, 60));

		// button 2
		final JButton rightConnectButton = new JButton(" Connect Camera 2");
		final JButton rightDisconnectButton = new JButton(
				" Disconnect Camera 2");
		rightDisconnectButton.setEnabled(false);

		rightConnectButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					createConnection(1, getRightFieldText());
				} catch (NullPointerException exception) {
					JOptionPane.showMessageDialog(frame,
							"Could not establish a connection!",
							"Connection Error", JOptionPane.ERROR_MESSAGE);
					return;
				} catch (IllegalArgumentException exception) {
					JOptionPane.showMessageDialog(frame,
							"The address provided was not a valid format!",
							"Format Error", JOptionPane.ERROR_MESSAGE);
					rightIPField.setText("172.0.0.1:6077");
					return;
				}
				rightConnectButton.setEnabled(false);
				rightDisconnectButton.setEnabled(true);
				rightIPField.setEnabled(false);
			}
		});

		rightDisconnectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				con.tearDown(1);
				rightConnectButton.setEnabled(true);
				rightDisconnectButton.setEnabled(false);
				rightIPField.setEnabled(true);
			}
		});
		rightConnectButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		rightDisconnectButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		rightConnectButton.setForeground(new Color(39, 174, 96));
		rightDisconnectButton.setForeground(new Color(231, 76, 60));

		// adding to leftBotP

		leftBotP.add(leftConnectButton);
		leftBotP.add(leftDisconnectButton);
		leftBotP.add(leftIPLabel);
		leftBotP.add(leftIPField);

		// adding to rightBotP

		rightBotP.add(rightConnectButton);
		rightBotP.add(rightDisconnectButton);
		rightBotP.add(rightIPLabel);
		rightBotP.add(rightIPField);

		// adding to botP

		botP.add(leftBotP);
		botP.add(rightBotP);

		// adding to main Panel
		mainP.add(topLabels);
		mainP.add(topP);
		mainP.add(delayPanel);
		mainP.add(botP);

		// adding to frame

		frame.add(mainP);
		//frame.setPreferredSize(new Dimension(1280, 600));
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();

		frame.setVisible(true);

	}

	private void createConnection(int id, String s) {
		if (!s.matches("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}):(\\d{1,5})")) {
			throw new IllegalArgumentException("Not a valid ip address");
		}
		String[] host = s.split(":");
		con.buildUp(host[0], Integer.parseInt(host[1]), id);
	}

	// updating leftTopLable
	public void updateleftTopLable(String text) {
		leftTopLabel.setText(text);
	}

	// updating rightTopLable
	public void updaterightTopLable(String text) {
		rightTopLabel.setText(text);
	}

	public String getLeftFieldText() {
		return leftIPField.getText();
	}

	public String getRightFieldText() {
		return rightIPField.getText();
	}

	private int unsynchCounter1 = 0;

	public void setDelayText1(int delay) {
		if (delay < Client.IMAGE_DELAY + 25) {
			unsynchCounter1 = 0;
			delayCamera1.setText("Synchronized     Delay: " + delay);
		} else {
			unsynchCounter1++;
			if (unsynchCounter1 > 2) {
				delayCamera1.setText("Not Synchronized     Delay: " + delay);
			} else {
				delayCamera1.setText("Synchronized     Delay: " + delay);
			}
		}
	}

	private int unsynchCounter2 = 0;

	public void setDelayText2(int delay) {
		if (delay < Client.IMAGE_DELAY + 25) {
			unsynchCounter2 = 0;
			delayCamera2.setText("Synchronized     Delay: " + delay);
		} else {
			unsynchCounter2++;
			if (unsynchCounter2 > 2) {
				delayCamera2.setText("Not Synchronized     Delay: " + delay);
			} else {
				delayCamera2.setText("Synchronized     Delay: " + delay);
			}
		}
	}

	public void run() {
		while (true) {
			SyncedImageBuffer.Node data = buff.getLatestImage(); // Wait until a
																	// new image
																	// arrives
			if (data == null)
				continue;

			switch (data.id) {
			case 0:
				leftImage.refresh(data.data);

				setDelayText1((int) (System.currentTimeMillis() - data.ts));
				break;
			case 1:
				rightImage.refresh(data.data);
				setDelayText2((int) (System.currentTimeMillis() - data.ts));
				break;
			}

			// Check if connections are still open
		}
	}
}

// Image panel
class ImagePanel extends JPanel {
	ImageIcon icon;
	JLabel label;

	public ImagePanel() {
		super();
		icon = new ImageIcon();
		setBackground(Color.BLACK);
		label = new JLabel(icon);

		add(label, BorderLayout.CENTER);
		this.setSize(0, 0);
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setAlignmentY(Component.TOP_ALIGNMENT);
	}

	public void refresh(byte[] data) {
		if (data == null)
			return;
		Image theImage = getToolkit().createImage(data);
		getToolkit().prepareImage(theImage, -1, -1, null);
		icon.setImage(theImage);

		icon.paintIcon(this, this.getGraphics(), 0, 0);
	}

}