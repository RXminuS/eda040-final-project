package skeleton.client;

import skeleton.server.ImagePackage;

public class SyncedImageBuffer {
	public class Node{
		public byte[] data;
		public long ts;
		private Node next;
		public int id;
	}
	
	private class SyncedBuffer{
		private Node root;
		
		SyncedBuffer(){
			root = new Node();
			root.next = null;
		}
		
		synchronized public void insert(ImagePackage image, int id){
			Node node = new Node();
			node.data = image.imageBuff;
			node.ts = image.timeStamp;
			node.id = id;
			insertRecursive(root, node);
		}
		
		synchronized public void insertRecursive(Node parent, Node child){
			if(parent.next == null){ //Reached the end of the list
				parent.next = child;
				child.next = null;
			}else if(parent.next.ts > child.ts){ //we should be inserted before an older image
				Node tmp = parent.next;
				parent.next = child;
				child.next = tmp;
				if(parent == root){ //image is the new latest image
					notifyAll();
				}
			}else{ // we should be further down the list
				insertRecursive(parent.next, child);
			}
		}
		
		synchronized public boolean hasNext(){
			return root.next != null;
		}
		
		synchronized public Node getFront(){
			return root.next;
		}
		
		synchronized public void popFront(){
			Node tmp = root.next;
			root.next = tmp.next;
		}
	}
	private SyncedBuffer buffer;
	
	public SyncedImageBuffer(){
		//Create two arrays (One per server)
		buffer = new SyncedBuffer();
	}
	
	private boolean isDeprecated(long timestamp){
		boolean retval = ((timestamp - System.currentTimeMillis()) > 0)?(true):(false);
		return retval;
	}
	
	synchronized public void putImage(ImagePackage image, int serverID){
		buffer.insert(image, serverID);
		/*return;
		for(int i= 0; i < bufferOne.length; i++){
			if(image.timeStamp < bufferOne[i].timeStamp ){
				
			}
		}*/
		
	}
	
	synchronized public Node getLatestImage(){
		Node last = null;
		while(buffer.hasNext()){
			Node n = buffer.getFront();
			long sleep = n.ts + Client.IMAGE_DELAY - System.currentTimeMillis();
			if(sleep > 0){
				try {
					wait(sleep);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(n == buffer.getFront()){
				buffer.popFront();
				return n;
			}
			last = n;
		}
		return last;
	}
}
